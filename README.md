# EjercicioJPA_JPQL

Este es un ejercicio de ejemplo del JPA y JPQL.

## Contenido


## Requisitos
- Maven.
- JDK 11 ó superior.
- MySQL Server 5 ó superior.

## Test and Deploy

Ejecutar pruebas unitarias. Las pruebas unitarias se encuentran en la carpeta de test.

***

## Instalación
* Compile el proyecto con el IDE de su preferencia.
* Ejecute el main ubicado en com.alfaCentauri/client/jpql/PruebaJPQL.jar

## Authors and acknowledgment
[GitLab: alfaCentauri1](https://gitlab.com/alfaCentauri1)

## License
GNU version 3.

## Project status
Terminado.